package au.com.ricoh.interview.printstream;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.stream.Stream;

public class VariableModifierService {
	public static final String UNEXPECTED_WRITE_ERROR_MSG = "An unexpected error occurred while writing to the output file. Check that the file is not being accessed by another person or program.";
	public static final String VARIABLE_SEPARATOR = "=";
	public static final String VARIABLE_QUOTATION = "\"";
	public static final String PJL_HEADER_VARIABLE_DECLARATION_PREFIX = "@PJL SET";
	public static final String VARIABLE_PREFIX = " ";
	
	/**
	 * Replace all values for the given variable name in the given stream. Write out all lines (modified or not) to the given buffered writer.
	 * Only lines beginning with the given linePrefix will be modified. This enables the client to target specific lines for modification.
	 * Assume lines will end with the end quotation of the variable value, meaning we need not conserve any part of the string past that point.
	 * 
	 * @param in the stream of strings to replace values from
	 * @param out the buffered writer to write lines to
	 * @param varName the name of the variable whose value we are trying to change
	 * @param value the new value of the given variable name
	 */
	public void replaceValues(Stream<String> in, BufferedWriter out, String linePrefix, String varName, String value) {
    	in.forEachOrdered(line -> {
			try {
				line = changeValue(line, linePrefix, varName, value);
				out.write(line);
				out.write('\n');
			} catch (IOException e) {
				System.out.println(UNEXPECTED_WRITE_ERROR_MSG);
				System.out.println(e.getMessage());
			}
		});
	}
	
	/**
	 * Similar to replaceValues, except only look to modify lines marked as PJL and using the SET keyword to avoid modifying lines using the declared variable.
	 * 
	 * @param in the stream of strings to replace values from
	 * @param out the buffered writer to write lines to
	 * @param varName the name of the variable whose value we are trying to change
	 * @param value the new value of the given variable name
	 */
	public void replaceHeaderDeclarationValues(Stream<String> in, BufferedWriter out, String varName, String value) {
		replaceValues(in, out, PJL_HEADER_VARIABLE_DECLARATION_PREFIX, varName, value);
	}
	
	/**
	 * Try to change the value of a variable given a line like - ^*<LINE_PREFIX>*<VARIABLE_NAME>*<SEPARATOR>*=*"<VALUE>"$
	 * Assume the symbols = and " will be used as shown on the line above.
	 * 
	 * @param line the line to modify
	 * @param variableName the name of the variable to search for
	 * @param separator the separator used to delimit the variable name and variable value
	 * @param value the new value of the variable
	 * @return a modified string containing the new value
	 */
	public String changeValue(String line, String linePrefix, String variableName, String value) {
		if (line.startsWith(linePrefix)) {
			int varIndex = line.indexOf(VARIABLE_PREFIX + variableName);
			if (varIndex >= 0) {
				StringBuilder newLine = new StringBuilder(line.substring(0, varIndex + variableName.length() + VARIABLE_PREFIX.length()));
				newLine.append(VARIABLE_SEPARATOR);
				newLine.append(VARIABLE_QUOTATION);
				newLine.append(value);
				newLine.append(VARIABLE_QUOTATION);
				return newLine.toString();
			}
		}
		return line;
	}
}
