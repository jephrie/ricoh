package au.com.ricoh.interview.printstream;

public class Runner {
	private static final String SUCESS_MESSAGE = "Completed with no errors.";
	private static final String ERROR_MSG_PREFIX = "Exiting program. ";
	
	public static void main( String[] args )
    {
		String errorMsg = null;
		App app = new App();
		
    	// validate input early to catch early termination cases.
		errorMsg = app.validateInput(args);
    	if (errorMsg != null) {
    		System.out.println(ERROR_MSG_PREFIX + errorMsg);
    		return;
    	}
    	
    	errorMsg = app.run(args);
		if (errorMsg != null) {
			System.out.println(ERROR_MSG_PREFIX + errorMsg);
			return;
		}
		
		System.out.println(SUCESS_MESSAGE);
    }
}
