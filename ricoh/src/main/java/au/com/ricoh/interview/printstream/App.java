package au.com.ricoh.interview.printstream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class App {
	public static final String ILLEGAL_ARGUMENT_COUNT_ERROR_MSG = "Four arguments required. 1.Path to print stream file 2.Path to modified print stream file 3.Name of variable to modify(case insensitive) 4.New value";
	
	public static final String CANT_OPEN_IN_FILE_ERROR_MSG = "Could not open print stream file to modify. Please check that the file exists and has the required permissions and try again.";
	public static final String IN_FILE_PATH_CONVERSION_ERROR_MSG = "Could not convert print stream file path to UTF-8.";
	public static final String UNABLE_TO_READ_IN_FILE_ERROR_MSG = "Could not read the print stream file. Check file permissions and try again.";
	
	public static final String CANT_WRITE_OUT_FILE_ERROR_MSG = "Could not write to output file. Please check permissions on the given output file.";
	public static final String OUT_FILE_EXISTS_ERROR_MSG = "Output file already exists. Please provide another output file path.";
	public static final String OUT_FILE_PATH_CONVERSION_ERROR_MSG = "Could not convert output file path to UTF-8.";
	public static final String UNABLE_TO_ACCESS_OUT_FILE_ERROR_MSG = "Could not access the output file. Check that the file does not exist and has the write permissions and try again.";
	
	public static final String INVALID_VARIABLE_NAME_ERROR_MSG = "The variable name cannot be null or empty.";
	public static final String INVALID_VALUE_ERROR_MSG = "The new value cannot be null.";
	
	public static final String ERROR_MSG_CAUSE_PREFIX = " Cause: ";
	
	public static final String FILE_ENCODING = "Cp1252";
	
	private VariableModifierService variableModifierService = new VariableModifierService();
	
    public String validateInput(String[] args) {
    	if (args.length != 4) {
    		return ILLEGAL_ARGUMENT_COUNT_ERROR_MSG;
    	}
    	
    	Path inPath = Paths.get(args[0]);
    	if (!Files.exists(inPath)) {
    		return CANT_OPEN_IN_FILE_ERROR_MSG;
    	}
    	
    	Path outPath = Paths.get(args[1]);
    	if (Files.exists(outPath)) {
    		return OUT_FILE_EXISTS_ERROR_MSG;
    	}
    	
    	String varName = args[2];
    	if (varName == null || varName.isEmpty()) {
    		return INVALID_VARIABLE_NAME_ERROR_MSG;
    	}
    	
    	String newVarValue = args[3];
    	if (newVarValue == null) {
    		return INVALID_VALUE_ERROR_MSG;
    	}
    	
    	return null;
    }
    
    public String run(String[] args) {
    	try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(args[0])), FILE_ENCODING))) {
    		Stream<String> in = reader.lines();
    		String varName = args[2];
    		String newVarValue = args[3];
			
    		try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(args[1])), FILE_ENCODING))) {
    			variableModifierService.replaceHeaderDeclarationValues(in, out, varName, newVarValue);
    			
    		} catch (InvalidPathException ipe) {
    			return OUT_FILE_PATH_CONVERSION_ERROR_MSG + ERROR_MSG_CAUSE_PREFIX + ipe.getMessage();
    		} catch (SecurityException se) {
    			return CANT_WRITE_OUT_FILE_ERROR_MSG + ERROR_MSG_CAUSE_PREFIX + se.getMessage();
    		} catch (UnsupportedOperationException uoe) {
    			// Not using options when creating buffered writer. Should not happen.
    			return ERROR_MSG_CAUSE_PREFIX + uoe.getMessage();
    		} catch (IOException e) {
    			return UNABLE_TO_ACCESS_OUT_FILE_ERROR_MSG + ERROR_MSG_CAUSE_PREFIX + e.getMessage();
    		}
    		
    	} catch (InvalidPathException ipe) {
    		return IN_FILE_PATH_CONVERSION_ERROR_MSG + ERROR_MSG_CAUSE_PREFIX + ipe.getMessage();
		} catch (SecurityException se) {
			return UNABLE_TO_READ_IN_FILE_ERROR_MSG + ERROR_MSG_CAUSE_PREFIX + se.getMessage();
		} catch (IOException e) {
			return CANT_OPEN_IN_FILE_ERROR_MSG + ERROR_MSG_CAUSE_PREFIX + e.getMessage();
		}
    	return null;
    }
    
}
