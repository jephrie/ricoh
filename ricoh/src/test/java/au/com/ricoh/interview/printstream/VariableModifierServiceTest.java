package au.com.ricoh.interview.printstream;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class VariableModifierServiceTest {
	// input has exactly zero occurrences of variable
    @Test
    public void variableNotInInputTest()
    {
    	VariableModifierService variableModifier = new VariableModifierService();
        List<String> in = new ArrayList<String>();
        in.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        in.add("@PJL SET DONT_CHANGE_ME = \"original\"");
        in.add("Integer tortor nibh, tincidunt vitae quam sed, convallis convallis ligula.");
        String linePrefix = "@PJL SET";
        String variableName = "CHANGE_ME";
        String value = "changed";
        
        for (String line : in) {
        	String newLine = variableModifier.changeValue(line, linePrefix, variableName, value);
        	Assert.assertEquals(line, newLine);
        }
    }
    
    // input has exactly one occurrence of variable
    @Test
    public void oneVariableInInputTest()
    {
    	VariableModifierService variableModifier = new VariableModifierService();
        List<String> in = new ArrayList<String>();
        in.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        in.add("@PJL SET CHANGE_ME = \"original\"");
        in.add("Integer tortor nibh, tincidunt vitae quam sed, convallis convallis ligula.");
        String linePrefix = "@PJL SET";
        String variableName = "CHANGE_ME";
        String value = "changed";
        int changeIndex = 1;
        String expected = "@PJL SET CHANGE_ME=\"changed\"";
        
        for (int i=0; i < in.size(); i++) {
        	String line = in.get(i);
        	String newLine = variableModifier.changeValue(line, linePrefix, variableName, value);
        	if (i == changeIndex) {
        		Assert.assertEquals(expected, newLine);
        	} else {
        		Assert.assertEquals(line, newLine);
        	}
        }
    }
    
    // input has multiple occurrences of variable that needs changing
    @Test
    public void multipleVariableInInputTest()
    {
    	VariableModifierService variableModifier = new VariableModifierService();
        List<String> in = new ArrayList<String>();
        in.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        in.add("@PJL SET CHANGE_ME = \"original\"");
        in.add("@PJL SET DONT_CHANGE_ME = \"original\"");
        in.add("@PJL SET CHANGE_ME = \"original\"");
        in.add("@PJL ENTER LANGUAGE = PCL");
        in.add("Integer tortor nibh, tincidunt vitae quam sed, convallis convallis ligula.");
        String linePrefix = "@PJL SET";
        String variableName = "CHANGE_ME";
        String value = "changed";
        boolean[] changed = {false, true, false, true, false, false};
        String expected = "@PJL SET CHANGE_ME=\"changed\"";
        
        for (int i=0; i < in.size(); i++) {
        	String line = in.get(i);
        	String newLine = variableModifier.changeValue(line, linePrefix, variableName, value);
        	if (changed[i]) {
        		Assert.assertEquals(expected, newLine);
        	} else {
        		Assert.assertEquals(line, newLine);
        	}
        }
    }
    
    // nested @PJL SET in @PJL COMMENT is not modified
    @Test
    public void variableInCommentTest() {
    	VariableModifierService variableModifier = new VariableModifierService();
        List<String> in = new ArrayList<String>();
        in.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        in.add("@PJL COMMENT @PJL SET CHANGE_ME");
        in.add("Integer tortor nibh, tincidunt vitae quam sed, convallis convallis ligula.");
        String linePrefix = "@PJL SET";
        String variableName = "CHANGE_ME";
        String value = "changed";
        
        for (int i=0; i < in.size(); i++) {
        	String line = in.get(i);
        	String newLine = variableModifier.changeValue(line, linePrefix, variableName, value);
    		Assert.assertEquals(line, newLine);
        }

    }

    // right hand assignment of a previous variable WILL be modified. unsure as to whether this is possible in @PJL.
    @Test
    public void variableAsAssignmentTest() {
    	VariableModifierService variableModifier = new VariableModifierService();
        List<String> in = new ArrayList<String>();
        in.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        in.add("@PJL SET COPIES = CHANGE_ME");
        in.add("Integer tortor nibh, tincidunt vitae quam sed, convallis convallis ligula.");
        String linePrefix = "@PJL SET";
        String variableName = "CHANGE_ME";
        String value = "changed";
        int changeIndex = 1;
        String expected = "@PJL SET COPIES = CHANGE_ME=\"changed\"";
        
        for (int i=0; i < in.size(); i++) {
        	String line = in.get(i);
        	String newLine = variableModifier.changeValue(line, linePrefix, variableName, value);
        	if (i == changeIndex) {
        		Assert.assertEquals(expected, newLine);
        	} else {
        		Assert.assertEquals(line, newLine);
        	}
        }
    }

    // leading and trailing whitespace is NOT removed before resolution/modification.
    @Test
    public void redundantSpaceTest() {
    	VariableModifierService variableModifier = new VariableModifierService();
        List<String> in = new ArrayList<String>();
        in.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        in.add("   @PJL SET CHANGE_ME = \"original\" ");
        in.add("Integer tortor nibh, tincidunt vitae quam sed, convallis convallis ligula.");
        String linePrefix = "@PJL SET";
        String variableName = "CHANGE_ME";
        String value = "changed";
        
        for (int i=0; i < in.size(); i++) {
        	String line = in.get(i);
        	String newLine = variableModifier.changeValue(line, linePrefix, variableName, value);
    		Assert.assertEquals(line, newLine);
        }
    }
}
