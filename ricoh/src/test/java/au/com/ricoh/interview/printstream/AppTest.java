package au.com.ricoh.interview.printstream;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.Assert;

public class AppTest {
	private final static String EXISTING_FILE_PATH = "existing.txt";
	private static final String NON_EXISTANT_OUTPUT_FILE_PATH = "out.txt";
	
	// ensure this file is unable to be read or modified on your file system.
	private static final String NO_ACCESS_FILE_PATH = "noAccess.txt";
	private static final String NO_ACCESS_ERROR_POSTFIX = " (Access is denied)";
	
	private static App app;
	
	@BeforeClass
	public static void setup() throws IOException {
		app = new App();
	}
	
	@AfterClass
	public static void cleanup() {
		File out = new File(NON_EXISTANT_OUTPUT_FILE_PATH);
		out.delete();
	}
	
	@Test
	public void validArgsTest() {
		String[] args = {EXISTING_FILE_PATH, NON_EXISTANT_OUTPUT_FILE_PATH, "c", "d"};
		String errorMsg = app.validateInput(args);
		Assert.assertEquals(null, errorMsg);
	}

	@Test
	public void inputFileDoesNotExistTest() {
		String[] args = {"in.txt", NON_EXISTANT_OUTPUT_FILE_PATH, "c", "d"};
		String errorMsg = app.validateInput(args);
		Assert.assertEquals(App.CANT_OPEN_IN_FILE_ERROR_MSG, errorMsg);
	}

	@Test
	public void outputFileExistsTest() {
		String[] args = {EXISTING_FILE_PATH, EXISTING_FILE_PATH, "c", "d"};
		String errorMsg = app.validateInput(args);
		Assert.assertEquals(App.OUT_FILE_EXISTS_ERROR_MSG, errorMsg);
	}
	
	@Test
	public void incorrectArgCountTest() {
		String[] args = {EXISTING_FILE_PATH, NON_EXISTANT_OUTPUT_FILE_PATH, "c"};
		String errorMsg = app.validateInput(args);
		Assert.assertEquals(App.ILLEGAL_ARGUMENT_COUNT_ERROR_MSG, errorMsg);
	}
	
	@Test
	public void nullVariableNameTest() {
		String[] args = {EXISTING_FILE_PATH, NON_EXISTANT_OUTPUT_FILE_PATH, null, "d"};
		String errorMsg = app.validateInput(args);
		Assert.assertEquals(App.INVALID_VARIABLE_NAME_ERROR_MSG, errorMsg);
	}
	
	@Test
	public void emptyVariableNameTest() {
		String[] args = {EXISTING_FILE_PATH, NON_EXISTANT_OUTPUT_FILE_PATH, "", "d"};
		String errorMsg = app.validateInput(args);
		Assert.assertEquals(App.INVALID_VARIABLE_NAME_ERROR_MSG, errorMsg);
	}
	
	@Test
	public void nullVarValueTest() {
		String[] args = {EXISTING_FILE_PATH, NON_EXISTANT_OUTPUT_FILE_PATH, "c", null};
		String errorMsg = app.validateInput(args);
		Assert.assertEquals(App.INVALID_VALUE_ERROR_MSG, errorMsg);
	}
	
	@Test
	public void noWriteAccessToOutFileTest() {
		String[] args = {EXISTING_FILE_PATH, NO_ACCESS_FILE_PATH, "c", "d"};
		String errorMsg = app.run(args);
		Assert.assertEquals(App.UNABLE_TO_ACCESS_OUT_FILE_ERROR_MSG + App.ERROR_MSG_CAUSE_PREFIX + NO_ACCESS_FILE_PATH + NO_ACCESS_ERROR_POSTFIX, errorMsg);
	}
	
	@Test
	public void noReadAccessToInFileTest() {
		String[] args = {NO_ACCESS_FILE_PATH, NON_EXISTANT_OUTPUT_FILE_PATH, "c", "d"};
		String errorMsg = app.run(args);
		Assert.assertEquals(App.CANT_OPEN_IN_FILE_ERROR_MSG + App.ERROR_MSG_CAUSE_PREFIX + NO_ACCESS_FILE_PATH + NO_ACCESS_ERROR_POSTFIX, errorMsg);
	}
	
}
